#!/bin/bash
# Brian Wilson <brian@wiltech.org>
# Git: https://gitlab.com/wiltech/wt-repo-tools
#

set -e

# Needed for GPG signing (ioctl issue for ssh usage)...
export GPG_TTY=$(tty)

WORK_DIR=$(pwd)
CONF_DIR=$(pwd)
LOG_FILE=/dev/null
DEBUG=false

REMOTE_USER=$(whoami)
REMOTE_PORT=
REMOTE_HOST=
REMOTE_REPO_DIR="/var/www/repo"

DIST_CODENAME=

SSH_PRIVATE_KEY_FILE="$HOME/.ssh/id_rsa"
GPG_KEY_ID=$PUBKEY_ID

ARCH=amd64

if [[ $DEBUG == true ]]; then
    set -uo pipefail
    trap 's=$?; _msg_error "$0: Error on line $LINENO": $BASH_COMMAND" $s' ERR
fi

# Log information to file and stdout
# $1: Message
_msg_info() {
    if [[ ! -f "$LOG_FILE" ]]; then
        mkdir -p $(dirname $LOG_FILE)
        touch $LOG_FILE
    fi

    echo -e "I: ${1}" | tee -a $LOG_FILE
}

# Log error information and exit script
# $1: Error message
# $2: Exit code
_msg_error() {
    if [[ ! -f $LOG_FILE ]]; then
        mkdir -p $(dirname $LOG_FILE)
        touch $LOG_FILE
    fi
    echo -e "E: ${1} (exit ${2})" | tee -a $LOG_FILE
    exit ${2}
}

# Commands to run on script exit
# Usage: defer '<command>'
exit_cmd=""
defer() { exit_cmd="$@; $exit_cmd"; }
trap 'bash -c "$exit_cmd"' EXIT

show_deploy_vars() {
    _msg_info "DEPLOY VARIABLES"
    _msg_info "DIST_CODENAME:\t$DIST_CODENAME"
    _msg_info "ARCH:\t\t$ARCH"
    _msg_info "PUBKEY_ID:\t\t$GPG_KEY_ID"
    _msg_info "REMOTE_USER:\t\t$REMOTE_USER"
    _msg_info "REMOTE_HOST:\t\t$REMOTE_HOST"
    _msg_info "REMOTE_REPO_DIR:\t$REMOTE_REPO_DIR"
    _msg_info "REMOTE_PORT:\t\t$REMOTE_PORT"
    _msg_info
}

# Deploy packages to configured repo
run_deploy() {
    # Works but extremely annoying without ssh keys with no passphrase
    _msg_info "Running deploy..."
    local rebuild_only_mode=false
    for i in "$@"; do
        case $i in
            -a=*|--arch=*)
                ARCH="${i#*=}"
                shift
                ;;
            -k=*|--gpg-id=*)
                GPG_KEY_ID="${i#*=}"
                shift
                ;;
            -u=*|--user=*)
                REMOTE_USER="${i#*=}"
                shift
                ;;
            -p=*|--port=*)
                REMOTE_PORT="${i#*=}"
                shift
                ;;
            -h=*|--host=*)
                REMOTE_HOST="${i#*=}"
                shift
                ;;
            -d=*|--repo-dir=*)
                REMOTE_REPO_DIR="${i#*=}"
                shift
                ;;
            -c=*|--code-name=*)
                DIST_CODENAME="${i#*=}"
                shift
                ;;
            --rebuild)
                rebuild_only_mode=true
                ARCH="NA"
                _msg_info "=== REBUILD MODE ==="
                shift
                ;;
            --help)
                show_deploy_help
                exit 1
                ;;
        esac
    done

    show_deploy_vars

    if [[ "$rebuild_only_mode" = false ]]; then
        _msg_info "Transfering packages..."
        scp -rqCP $REMOTE_PORT $WORK_DIR/*.deb $REMOTE_USER@$REMOTE_HOST:$REMOTE_REPO_DIR/$DIST_CODENAME/$ARCH
    fi

    _msg_info "Updating package archive... (this may take some time)"
    ssh -p $REMOTE_PORT $REMOTE_USER@$REMOTE_HOST "cd $REMOTE_REPO_DIR/$DIST_CODENAME; apt-ftparchive --arch $ARCH packages . > $REMOTE_REPO_DIR/$DIST_CODENAME/Packages; gzip -k -f $REMOTE_REPO_DIR/$DIST_CODENAME/Packages"

    _msg_info "Updating release info..."
    ssh -p $REMOTE_PORT $REMOTE_USER@$REMOTE_HOST "apt-ftparchive release $REMOTE_REPO_DIR/$DIST_CODENAME > $REMOTE_REPO_DIR/$DIST_CODENAME/Release"

    _msg_info "Preparing to sign release info locally..."
    _tmp_dir=$(mktemp -d)
    defer "rm -rf $_tmp_dir"

    _msg_info "Downloading release file..."
    scp -qCP $REMOTE_PORT $REMOTE_USER@$REMOTE_HOST:$REMOTE_REPO_DIR/$DIST_CODENAME/Release $_tmp_dir

    _msg_info "Signing release file..."
    gpg --default-key $GPG_KEY_ID -abs -o $_tmp_dir/Release.gpg $_tmp_dir/Release &> /dev/null
    gpg --default-key $GPG_KEY_ID --clearsign -o $_tmp_dir/InRelease $_tmp_dir/Release &> /dev/null

    _msg_info "Uploading release signatures back to $REMOTE_HOST..."
    scp -rqCP $REMOTE_PORT "$_tmp_dir/Release.gpg" "$_tmp_dir/InRelease" $REMOTE_USER@$REMOTE_HOST:$REMOTE_REPO_DIR/$DIST_CODENAME

    _msg_info "Done."
}

run_clean() {
    _msg_info "Starting clean..."
    make clean
}

run_build() {
    _msg_info "Starting build..."
    make pkg
}

show_help() {
    echo "Usage: $0 <sub_command>"
    echo -e "\tb|build\t\tBuild project"
    echo -e "\tc|clean\t\tClean project directory"
    echo -e "\td|deploy\tUpdate the configured APT repository"
    echo -e "\t-h|--help\tShow this help message."
}

show_deploy_help() {
    echo "Usage: $0 deploy [args...]"
    echo -e "\t-a=|--arch=\t\tBinary arch of package"
    echo -e "\t-k=|--gpg-id=\t\tYour GPG key's ID"
    echo -e "\t-u=|--user=\t\tRemote SSH user"
    echo -e "\t-p=|--port=\t\tRemote SSH port"
    echo -e "\t-c=|--code-name=\tAPT Distro coden-name"
    echo -e "\t-h=|--host=\t\tRemote SSH host"
    echo -e "\t-d=|--remote-dir=\tRemote deployment directory"
    echo -e "\t--rebuild\t\tRebuild the repository without transfering packages"
}

if [[ ! -v "$1" ]]; then
    case $1 in
        b|build)
            run_build "$@"
            ;;
        c|clean)
            run_clean "$@"
            ;;
        d|deploy)
            run_deploy "$@"
            #parse_deploy_args "$@"
            ;;
        -h|--help)
            show_help
            ;;
        *)
            _msg_error "Usage: $0 --help" 1
            ;;
    esac
else
    _msg_error "Usage: $0 --help" 1
fi
